from node as build
add app /app
workdir /app
run yarn install && yarn build

from node
workdir /app
copy --from=build /app/dist /app/dist
run yarn install --production

cmd yarn start
