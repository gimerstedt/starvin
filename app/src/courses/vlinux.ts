// process from markdown perhaps?
const course = [
  { content: `desc kmom01`, embeds: [] },
  {
    content: `__**Vlinux Kmom02:**__`,
    embeds: [
      {
        title: 'kmom2',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02',
      },
      {
        title: 'Läsanvisningar',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#lasanvisningar',
      },
      {
        title: 'Kurslitteratur',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#kurslitteratur',
      },
      {
        title: 'Artiklar',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#artiklar',
      },
      {
        title: 'Video',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#video',
      },
      {
        title: 'Övningar & Uppgifter',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#ovningar_uppgifter',
      },
      {
        title: 'Övningar',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#ovningar',
      },
      {
        title: 'Uppgifter',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#uppgifter',
      },
      {
        title: 'Resultat & Redovisning',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#resultat_redovisning',
      },
      {
        title: 'Revision history',
        url: 'https://dbwebb.se/kurser/vlinux-v1/kmom02#revision',
      },
    ],
  },
  { content: `desc kmom03`, embeds: [] },
  { content: `desc kmom04`, embeds: [] },
]

export default function vlinux(mom: string) {
  const momIdx = parseInt(mom, 10) - 1
  const { content, embeds } = course[momIdx]
  return { content, embeds }
}
