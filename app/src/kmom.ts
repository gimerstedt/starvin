import { Message } from 'discord.js'
import vlinux from './courses/vlinux'

export default async function kmom(msg: Message, ...args: string[]) {
  const [course, mom] = args

  switch (course) {
    case 'vlinux':
      const ret = vlinux(mom)
      if (ret) await msg.channel.send(ret)
      return
    default:
      return
  }
}
