import dotenv from 'dotenv'
import { Client } from 'discord.js'
import kmom from './kmom'
import env from './env'

dotenv.config()

const token = process.env.TOKEN

// ???
const bot = new Client({ intents: ['GUILDS', 'GUILD_MESSAGES'] })

async function main() {
  await bot.login(token).catch((e) => {
    console.error(e)
  })

  bot.on('ready', () => {
    console.info('ready')
  })

  bot.on('messageCreate', async (msg) => {
    if (!msg.content.startsWith('!')) return
    const str = msg.content.slice(1)

    const [cmd, ...rest] = str.split(' ')

    // read/parse course data, loop to find match instead

    switch (cmd) {
      case 'ping':
        await msg.reply({ content: 'pong' })
      case 'kmom':
        await kmom(msg, ...rest)
        return
      case 'env':
        await env(msg, ...rest)
      default:
        return
    }
  })
}

main()
