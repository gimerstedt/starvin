import { Message } from 'discord.js'

interface Env {
  os: string
}

// primitive storage
const users = new Map<string, Env>()

function formatEnv(e: Env) {
  return `
  OS: ${e.os}
  `
}

export default async function env(msg: Message, ...args: string[]) {
  if (args.length === 0) {
    const user = msg.author.username
    const env = users.get(user)
    if (env) await msg.reply(formatEnv(env))
    else await msg.reply(`no env found for ${user} :(`)
  } else if (args.length === 1) {
    const user = args[0]
    const env = users.get(user)
    if (env) await msg.reply(formatEnv(env))
    else await msg.reply(`no env found for ${user} D:`)
  } else {
    const user = msg.author.username
    const hadEnv = users.has(user)
    users.set(user, { os: args.join(' ') })
    await msg.reply(hadEnv ? 'Env updated' : 'Env created, ty!')
  }
}
